/**
 * app 公共封装
 */
class HzyAdminUIClass {
    constructor() {
        this.writeHead();
    }

    /**
     * 加载公共资源文件到页面中
     * 
     */
    writeHead() {
        document.write(`

<head>
    <script>
        var _hmt = _hmt || [];
        (function () {
            var hm = document.createElement("script");
            hm.src = "https://hm.baidu.com/hm.js?90d9b1016c84f436fc9b4b318f5a0ff9";
            var s = document.getElementsByTagName("script")[0];
            s.parentNode.insertBefore(hm, s);
        })();
    </script>
    <meta name="keywords" content="hzyadmin,hzy,HZY,HZY.AdminRazor,hzyadminspa,HZY.AdminSpa">
    <meta charset="UTF-8">
    <!-- 避免IE使用兼容模式 -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
    <!-- 移动设备 viewport -->
    <meta name="viewport"
        content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no,minimal-ui">
    <!-- 360浏览器默认使用Webkit内核 -->
    <meta name="renderer" content="webkit">
    <!-- 禁止搜索引擎抓取 -->
    <meta name="robots" content="nofollow">
    <!-- 禁止百度SiteAPP转码 -->
    <meta http-equiv="Cache-Control" content="no-siteapp">
    <!-- Chrome浏览器添加桌面快捷方式（安卓） -->
    <meta name="mobile-web-app-capable" content="yes">
    <!-- Safari浏览器添加到主屏幕（IOS） -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-title" content="">

    <link rel="shortcut icon" href="/favicon.ico">
    <!-- nprogress.css -->
    <link rel="stylesheet" href="/libs/nprogress/nprogress.css" />
    <!--element-plus-->
    <link rel="stylesheet" href="//unpkg.com/element-plus/dist/index.css" />
    <!--hzy-admin-ui-public-->
    <link href="/assets/styles/hzy-admin-ui.css" rel="stylesheet">
    <!--hzy-admin-ui-tools-->
    <link href="/assets/styles/hzy-admin-ui-tools.css" rel="stylesheet">
    <!--hzy-admin-ui-login-->
    <link href="/assets/styles/hzy-admin-ui-login.css" rel="stylesheet">

    <!-- jquery-3.2.1.min.js -->
    <script src="/libs/jquery/jquery-3.2.1.min.js"></script>
    <!-- layer -->
    <script src="/libs/layer/layer.js"></script>
    <!--nprogress-->
    <script src="/libs/nprogress/nprogress.js"></script>
    <!-- 导入 Vue 3 -->
    <script src="/libs/vue3.js"></script>
    <!--moment-->
    <!-- <script src="/libs/moment.min.js"></script> -->
    <!--element-plus-->
    <script src="//unpkg.com/element-plus"></script>
    <!--axios-->
    <script src="/libs/axios.min.js"></script>
    <!--tools-->
    <script src="/scripts/tools.js"></script>
    <!--httpClient-->
    <!-- <script src="/scripts/httpClient.js"></script> -->
</head>

`);
    }

    /**
     * 页面初始化 vue app
     * 
     * @param {*} elId 
     * @param {*} vueApp 
     */
    initVueApp(elId, vueApp) {
        vueApp
            .use(window.ElementPlus, { size: 'small', zIndex: 99999999 })
            .mount(elId)
            ;
    }
}

window.HzyAdminUI = new HzyAdminUIClass();
